import moment from 'moment';

export function espnLogo(teamAbrv, size) {
	return `http://a.espncdn.com/combiner/i?img=/i/teamlogos/nba/500/scoreboard/${teamAbrv.toLowerCase()}.png&h=${size}&w=${size}`;
}

export const todayString = moment()
	.format('YYYY-MMM-DD')
	.toUpperCase()
	.toString();

export const last30Days = moment()
	.subtract(30, 'days')
	.startOf('day')
	.format();

export const themeColors = {
	black: '#222629',
	grayLite: '#ddd',
	gray: '#6a6e71',
	grayDark: '#474A4F',
	white: '#fff',
	lime: '#87c232',
	olive: '#618930',
};

export const singleGameRecords = {
	assists: 53,
	steals: 27,
	blocks: 23,
	turnovers: 45,
	rebounds: 109,
};

export function smallestToLargest(propArg) {
	return function(a, b) {
		if (a[propArg] > b[propArg]) {
			return 1;
		} else if (b[propArg] > a[propArg]) {
			return -1;
		}
		return 0;
	};
}

export function largestToSmallest(propArg) {
	return function(a, b) {
		if (a[propArg] < b[propArg]) {
			return 1;
		} else if (b[propArg] < a[propArg]) {
			return -1;
		}
		return 0;
	};
}

export function winningPercentage(wins, losses) {
	return function(a, b) {
		if (a[wins] / a[losses] < b[wins] / b[losses]) {
			return 1;
		} else if (b[wins] / b[losses] < a[wins] / a[losses]) {
			return -1;
		}
		return 0;
	};
}
