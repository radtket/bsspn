import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import { save, load } from 'redux-localstorage-simple';

import rootReducer from './reducers/rootReducer';

import Home from './app/Home';
import TeamHome from './app/Team';
import NavigationMain from './components/NavigationMain';

const middleware = [logger, thunk, promise];

const store = createStore(rootReducer, load(), composeWithDevTools(applyMiddleware(...middleware, save())));
// const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));

const App = () => (
	<Provider store={store}>
		<Router>
			<div className="page-wrap">
				<NavigationMain />
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/team/:teamAbrv" component={TeamHome} />
				</Switch>
			</div>
		</Router>
	</Provider>
);

export default App;
