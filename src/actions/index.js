import moment from 'moment';
import apiHeaders from '../util/api';

export const GET_TEAM = 'GET_TEAM';
export const RESET_TEAM = 'RESET_TEAM';
export const GET_NEWS_BY_DATE = 'GET_NEWS_BY_DATE';
export const GET_STANDINGS = 'GET_STANDINGS';
export const GET_SCHEDULES = 'GET_SCHEDULES';
export const GET_LATEST_TEAM_BOXSCORE = 'GET_LATEST_TEAM_BOXSCORE';
export const GET_ARENAS = 'GET_ARENAS';
export const GET_ARENA_BY_ID = 'GET_ARENA_BY_ID';

const currentYear = moment()
	.utc()
	.format('YYYY')
	.toUpperCase();

export function getTeam(teamAbrv) {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/JSON/teams`, apiHeaders);
		const teams = await res.json();
		const team = await teams.find(item => item.Key === teamAbrv);
		return dispatch({
			type: GET_TEAM,
			data: team,
		});
	};
}

export function resetTeam() {
	return {
		type: RESET_TEAM,
	};
}

export function getNewsByDate(date) {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/JSON/NewsByDate/${date}`, apiHeaders);
		const news = await res.json();
		return dispatch({
			type: GET_NEWS_BY_DATE,
			data: news,
		});
	};
}

export function getStandings() {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/JSON/Standings/${currentYear}`, apiHeaders);
		const standings = await res.json();
		return dispatch({
			type: GET_STANDINGS,
			data: standings,
		});
	};
}

export function getSchedules() {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/JSON/Games/${currentYear}`, apiHeaders);
		const schedules = await res.json();
		return dispatch({
			type: GET_SCHEDULES,
			data: schedules,
		});
	};
}

export function getTeamGame(gameid) {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/JSON//BoxScore/${gameid}`, apiHeaders);
		const boxScore = await res.json();
		return dispatch({
			type: GET_LATEST_TEAM_BOXSCORE,
			data: boxScore,
		});
	};
}

export function getArenas() {
	return async function(dispatch) {
		const res = await fetch(`https://api.fantasydata.net/v3/nba/stats/json/Stadiums`, apiHeaders);
		const arenas = await res.json();
		return dispatch({
			type: GET_ARENAS,
			data: arenas,
		});
	};
}

export function getArenaByStadiumId(arenaId, obj) {
	return async function(dispatch) {
		const homeArena = await obj.find(item => item.StadiumID === arenaId);
		return dispatch({
			type: GET_ARENA_BY_ID,
			data: homeArena,
		});
	};
}
