import { GET_TEAM, RESET_TEAM, GET_LATEST_TEAM_BOXSCORE } from '../actions';

const initialState = {
	team: {},
	teamLoaded: false,
	teamBoxScore: {},
	teamBoxScoreLoaded: false,
};

export default function(state = initialState, action) {
	const { type, data } = action;
	switch (type) {
		case GET_TEAM:
			return {
				...state,
				team: data,
				teamLoaded: true,
			};
		case RESET_TEAM:
			return {
				...state,
				team: {},
				teamLoaded: false,
			};
		case GET_LATEST_TEAM_BOXSCORE:
			return {
				...state,
				teamBoxScore: data,
				teamBoxScoreLoaded: true,
			};
		default:
			return state;
	}
}
