import { GET_STANDINGS, GET_SCHEDULES, GET_ARENAS, GET_ARENA_BY_ID } from '../actions';

const initialState = {
	standings: [],
	standingsLoaded: false,
	schedules: [],
	schedulesLoaded: false,
	arenas: [],
	arenasLoaded: false,
	teamGameArena: {},
	teamGameArenaLoaded: false,
};

export default function(state = initialState, action) {
	const { type, data } = action;
	switch (type) {
		case GET_STANDINGS:
			return {
				...state,
				standings: data,
				standingsLoaded: true,
			};
		case GET_SCHEDULES:
			return {
				...state,
				schedules: data,
				schedulesLoaded: true,
			};
		case GET_ARENAS:
			return {
				...state,
				arenas: data,
				arenasLoaded: true,
			};
		case GET_ARENA_BY_ID:
			return {
				...state,
				teamGameArena: data,
				teamGameArenaLoaded: true,
			};
		default:
			return state;
	}
}
