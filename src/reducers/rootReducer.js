import { combineReducers } from 'redux';
import team from './reducer-team';
import news from './reducer-news';
import teams from './reducer-teams';

const rootReducer = combineReducers({
	team,
	news,
	teams,
});

export default rootReducer;
