import { GET_NEWS_BY_DATE } from '../actions';

const initialState = {
	news: [],
	newsLoaded: false,
};

export default function(state = initialState, action) {
	const { type, data } = action;
	switch (type) {
		case GET_NEWS_BY_DATE:
			return {
				...state,
				news: data,
				newsLoaded: true,
			};
		default:
			return state;
	}
}
