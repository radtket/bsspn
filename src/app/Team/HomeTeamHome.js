import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import moment from 'moment';

import { todayString, last30Days } from '../../util/helpers';
import { getNewsByDate, getSchedules } from '../../actions';
import PhotosComponent from '../../components/PhotosComponent';

// Images
import poster from '../../images/giannis.jpg';

// Components
import Card from '../../components/Card';
import Standings from '../../components/Standings';
import RecentGames from '../../components/RecentGames';
import TeamScoreboard from '../../components/TeamScoreboard';
import NewsLarge from '../../components/NewsLarge';

class HomeTeamHome extends Component {
	componentWillMount() {
		this.props.getNewsByDate(todayString);
		this.props.getSchedules();
		// if (!this.props.newsLoaded) {
		// 	this.props.getNewsByDate(todayString);
		// }
	}

	getTeamGameID() {
		const { schedules, teamAbrv } = this.props;
		const teamGame = [...schedules]
			.filter(
				team =>
					(team.HomeTeam === teamAbrv || team.AwayTeam === teamAbrv) &&
					team.Status !== 'Scheduled' &&
					moment(team.Day).format() >= last30Days
			)
			.reverse()
			.map(item => item.GameID);
		return teamGame[0];
	}

	getTeamGameStadiumID() {
		const { schedules, teamAbrv } = this.props;
		const teamGame = [...schedules]
			.filter(
				team =>
					(team.HomeTeam === teamAbrv || team.AwayTeam === teamAbrv) &&
					team.Status !== 'Scheduled' &&
					moment(team.Day).format() >= last30Days
			)
			.reverse()
			.map(item => item.StadiumID);
		return teamGame[0];
	}

	findTeamSchedule() {
		const { schedules, teamAbrv } = this.props;
		return schedules
			.filter(
				team =>
					(team.HomeTeam === teamAbrv || team.AwayTeam === teamAbrv) &&
					team.Status !== 'Scheduled' &&
					moment(team.Day).format() >= last30Days
			)
			.reverse();
	}

	renderNewsArticles() {
		const { news } = this.props;
		return news.map(post => (
			<NewsLarge
				key={post.NewsID}
				newsDate={post.Updated}
				newsImage={poster}
				newsImageDisc={post.Title}
				newsHeadline={post.Title}
				newsBody={post.Content}
				newsLink={post.Url}
			/>
		));
	}

	render() {
		return (
			<section className="small-section">
				<div className="container">
					<Grid>
						<Row className="show-grid">
							<Col xs={12} md={8}>
								<Card
									title="Game Scoreboard"
									component={
										<TeamScoreboard
											arenas={this.props.arenas}
											teamGameID={this.getTeamGameID()}
											teamGameStadiumId={this.getTeamGameStadiumID()}
											teamLogo={this.props.teamLogo}
											teamName={this.props.teamName}
											teamCity={this.props.teamCity}
											teamAbrv={this.props.teamAbrv}
											teamColor={this.props.teamColor}
										/>
									}
								/>
								{this.renderNewsArticles()}
							</Col>
							<Col xs={12} md={4}>
								<Card
									title="Standings"
									component={
										<Standings
											currentTeam={this.props.teamAbrv}
											teamConference={this.props.teamConference}
											teamDivision={this.props.teamDivision}
										/>
									}
								/>
								<Card
									title="Recent Games"
									component={<RecentGames teamSchedule={this.findTeamSchedule()} teamAbrv={this.props.teamAbrv} />}
								/>
								<Card title="Photos" component={<PhotosComponent />} />
							</Col>
						</Row>
					</Grid>
				</div>
			</section>
		);
	}
}

const mapStateToProps = state => ({
	news: state.news.news,
	newsLoaded: state.news.newsLoaded,
	schedules: state.teams.schedules,
	schedulesLoaded: state.teams.schedulesLoaded,
});

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			getNewsByDate,
			getSchedules,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(HomeTeamHome);

HomeTeamHome.propTypes = {
	teamConference: PropTypes.string,
	teamDivision: PropTypes.string,
	getNewsByDate: PropTypes.func.isRequired,
	news: PropTypes.arrayOf(PropTypes.object),
	// newsLoaded: PropTypes.bool.isRequired,
	arenas: PropTypes.arrayOf(PropTypes.object),
	teamLogo: PropTypes.string,
	teamAbrv: PropTypes.string.isRequired,
	teamName: PropTypes.string,
	teamCity: PropTypes.string,
	teamColor: PropTypes.string,
	schedules: PropTypes.arrayOf(PropTypes.object).isRequired,
	getSchedules: PropTypes.func.isRequired,
};

HomeTeamHome.defaultProps = {
	teamConference: 'Conference',
	teamDivision: 'Division',
	teamLogo: '',
	teamName: '',
	teamCity: '',
	teamColor: '',
	news: [],
	arenas: [],
};
