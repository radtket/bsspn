import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTeam, resetTeam, getArenas } from '../../actions';

// Components
import TeamNav from '../../components/TeamNav';
import TeamHero from '../../components/TeamHero';

import HomeTeamHome from './HomeTeamHome';

class TeamHome extends React.Component {
	componentWillMount() {
		const { match } = this.props;
		this.props.getTeam(match.params.teamAbrv);
		this.props.getArenas();
		// if (!this.props.teamLoaded) {
		// 	this.props.getTeam(match.params.teamAbrv);
		// }
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.match.params.teamAbrv !== nextProps.team.Key) {
			this.props.getTeam(nextProps.match.params.teamAbrv);
		}
		// console.log(nextProps.match.params.teamAbrv, nextProps.team.Key);
	}

	componentWillUnmount() {
		this.props.resetTeam();
	}

	render() {
		const { City, Conference, Division, Name, PrimaryColor, SecondaryColor, WikipediaLogoUrl } = this.props.team;
		const { url, params } = this.props.match;
		const { arenas } = this.props;
		return (
			<main>
				<TeamHero
					teamCity={City}
					teamName={Name}
					teamLogo={WikipediaLogoUrl}
					teamColor={PrimaryColor}
					altColor={SecondaryColor}
				/>
				<TeamNav />
				<Switch>
					<Route
						exact
						path={url}
						render={() => (
							<HomeTeamHome
								arenas={arenas}
								teamLogo={WikipediaLogoUrl}
								teamAbrv={params.teamAbrv}
								teamCity={City}
								teamName={Name}
								teamConference={Conference}
								teamDivision={Division}
								teamColor={PrimaryColor}
							/>
						)}
					/>
				</Switch>
			</main>
		);
	}
}

const mapStateToProps = state => ({
	team: state.team.team,
	teamLoaded: state.team.teamLoaded,
	arenas: state.teams.arenas,
	arenasLoaded: state.teams.arenasLoaded,
});

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			getTeam,
			resetTeam,
			getArenas,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(TeamHome);

TeamHome.propTypes = {
	match: PropTypes.shape({
		url: PropTypes.string.isRequired,
		params: PropTypes.shape({
			teamAbrv: PropTypes.string,
		}),
	}).isRequired,
	getTeam: PropTypes.func.isRequired,
	resetTeam: PropTypes.func.isRequired,
	teamLoaded: PropTypes.bool,
	team: PropTypes.shape({
		City: PropTypes.string,
		Conference: PropTypes.string,
		Division: PropTypes.string,
		Name: PropTypes.string,
		PrimaryColor: PropTypes.string,
		SecondaryColor: PropTypes.string,
		WikipediaLogoUrl: PropTypes.string,
	}).isRequired,
	getArenas: PropTypes.func.isRequired,
	arenas: PropTypes.arrayOf(PropTypes.object).isRequired,
};

TeamHome.defaultProps = {
	teamLoaded: false,
};
