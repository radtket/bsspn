import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import RecentGamesGame from './RecentGamesGame.jsx';

class RecentGames extends Component {
	renderTeamSchedules() {
		const { teamSchedule, teamAbrv } = this.props;
		return teamSchedule.map(game => (
			<RecentGamesGame
				key={game.GameID}
				isFinal={game.IsClosed}
				quarter={game.Quarter}
				isHome={game.HomeTeam === teamAbrv}
				opponentLogo={game.HomeTeam === teamAbrv ? game.AwayTeam : game.HomeTeam}
				opponentName={game.HomeTeam === teamAbrv ? game.AwayTeam : game.HomeTeam}
				opponentScore={game.HomeTeam === teamAbrv ? game.AwayTeamScore : game.HomeTeamScore}
				teamScore={game.HomeTeam === teamAbrv ? game.HomeTeamScore : game.AwayTeamScore}
			/>
		));
	}
	render() {
		return <ListGroup>{this.renderTeamSchedules()}</ListGroup>;
	}
}

export default RecentGames;

RecentGames.propTypes = {
	teamAbrv: PropTypes.string.isRequired,
	teamSchedule: PropTypes.arrayOf(PropTypes.object).isRequired,
};
