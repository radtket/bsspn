import React from 'react';
import { ListGroupItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import PropTypes from 'prop-types';
import { espnLogo } from '../../util/helpers';

const RecentGamesGame = props => (
	<LinkContainer to={`/team/${props.opponentName}`}>
		<ListGroupItem>
			<figure className="club-schedule__oponent--logo">
				<img src={props.opponentName === 'UTA' ? espnLogo('UTAH', 40) : espnLogo(`${props.opponentLogo}`, 40)} alt="" />
			</figure>
			<figcaption className="club-schedule__oponent--name">
				{props.isHome ? 'vs' : '@'} {props.opponentName}
			</figcaption>
			<div className="club-schedule__game">
				<span
					className={`club-schedule__game--result ${
						props.teamScore > props.opponentScore && props.isFinal ? 'club-schedule__game--result--winner' : false
					} ${props.teamScore < props.opponentScore && props.isFinal ? 'club-schedule__game--result--loser' : false}`}
				>
					{!props.isFinal ? `Q${props.quarter}` : false}
					{props.isFinal && props.teamScore > props.opponentScore ? 'W' : false}
					{props.isFinal && props.teamScore < props.opponentScore ? 'L' : false}
				</span>
				<span className="club-schedule__game--score">
					{props.teamScore > props.opponentScore
						? `${props.teamScore} - ${props.opponentScore}`
						: `${props.opponentScore} - ${props.teamScore}`}
				</span>
			</div>
		</ListGroupItem>
	</LinkContainer>
);

export default RecentGamesGame;

RecentGamesGame.propTypes = {
	opponentLogo: PropTypes.string.isRequired,
	opponentName: PropTypes.string.isRequired,
	opponentScore: PropTypes.number.isRequired,
	teamScore: PropTypes.number.isRequired,
	isHome: PropTypes.bool,
	isFinal: PropTypes.bool,
	quarter: PropTypes.string,
};

RecentGamesGame.defaultProps = {
	isHome: false,
	isFinal: false,
	quarter: '',
};
