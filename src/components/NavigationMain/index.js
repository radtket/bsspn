import React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

const NavigationMain = () => (
	<Navbar inverse collapseOnSelect>
		<Navbar.Header>
			<Navbar.Brand>
				<a href="#brand">React-Bootstrap</a>
			</Navbar.Brand>
			<Navbar.Toggle />
		</Navbar.Header>
		<Navbar.Collapse>
			<Nav pullRight>
				<NavItem eventKey={1} href="#" className="active">
					home
				</NavItem>
				<NavItem eventKey={2} href="#">
					scores
				</NavItem>
				<NavItem eventKey={3} href="#">
					schedule
				</NavItem>
				<NavItem eventKey={3} href="#">
					standings
				</NavItem>
				<NavDropdown eventKey={5} title="teams" id="basic-nav-dropdown">
					<MenuItem eventKey={5.1}>Action</MenuItem>
					<MenuItem eventKey={5.2}>Another action</MenuItem>
					<MenuItem eventKey={5.3}>Something else here</MenuItem>
					<MenuItem divider />
					<MenuItem eventKey={5.3}>Separated link</MenuItem>
				</NavDropdown>
			</Nav>
		</Navbar.Collapse>
	</Navbar>
);

export default NavigationMain;
