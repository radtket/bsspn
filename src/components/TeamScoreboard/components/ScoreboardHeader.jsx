import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const ScoreboardHeader = props => (
	<header className="game-result__meta">
		<span className="game-result__meta--date">{moment(props.date).format('MMMM Do YYYY')}</span>
		<span className="game-result__meta--venue text-center">{props.venue}</span>
		<span className="game-result__meta--location text-right">
			{props.isClosed
				? props.status
				: `Q${props.quarter} - ${props.timeRemainingMinutes}:${props.timeRemainingSeconds}`}
		</span>
	</header>
);

export default ScoreboardHeader;

ScoreboardHeader.propTypes = {
	date: PropTypes.string.isRequired,
	venue: PropTypes.string,
	isClosed: PropTypes.bool,
	status: PropTypes.string.isRequired,
	quarter: PropTypes.string,
	timeRemainingMinutes: PropTypes.number,
	timeRemainingSeconds: PropTypes.number,
};

ScoreboardHeader.defaultProps = {
	quarter: '',
	timeRemainingMinutes: 0,
	timeRemainingSeconds: 0,
	venue: '',
	isClosed: false,
};
