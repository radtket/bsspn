import React from 'react';
import PropTypes from 'prop-types';

const ScoreboardTeam = props => (
	<article
		className={`game-result__team game-result__team--away ${
			props.isHomeTeam ? 'game-result__team--home' : 'game-result__team--away'
		}`}
	>
		<div className="game-result__team--wrap">
			<figure className="game-result__team--logo">
				<img src={props.teamLogo} alt={`${props.teamCity} ${props.teamName} Logo`} />
			</figure>
			<figcaption className="game-result__team--info">
				<h5>
					<span className="game-result__team--city">{props.teamCity}</span>
					<span className="game-result__team--name">{props.teamName}</span>
				</h5>
				<span className="game-result__team--desc">{props.teamRecord}</span>
			</figcaption>
		</div>
		<span
			className={`game-result__score game-result__score--result ${
				props.isWinner ? 'game-result__score--result--winner' : 'game-result__score--result--loser'
			}`}
		>
			{props.teamGameScore}
		</span>
	</article>
);

export default ScoreboardTeam;

ScoreboardTeam.propTypes = {
	isHomeTeam: PropTypes.bool.isRequired,
	teamLogo: PropTypes.string.isRequired,
	teamCity: PropTypes.string.isRequired,
	teamName: PropTypes.string.isRequired,
	teamRecord: PropTypes.string.isRequired,
	isWinner: PropTypes.bool.isRequired,
	teamGameScore: PropTypes.number.isRequired,
};
