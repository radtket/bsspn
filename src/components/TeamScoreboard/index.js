import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import ScoreboardStatsProgress from './ScoreboardStatsProgress';
import ScoreboardTeam from './ScoreboardTeam';
import { getTeamGame, getArenaByStadiumId } from '../../actions';
import ScoreboardHeader from './components/ScoreboardHeader';
import { espnLogo } from '../../util/helpers';

class TeamScoreboard extends Component {
	componentWillUpdate(nextProps) {
		if (
			(!nextProps.teamBoxScoreLoaded && nextProps.teamGameID && nextProps.teamGameStadiumId) ||
			nextProps.teamGameID !== this.props.teamGameID ||
			!nextProps.teamGameArena
		) {
			this.props.getTeamGame(nextProps.teamGameID);
			this.props.getArenaByStadiumId(nextProps.teamGameStadiumId, this.props.arenas);
		}
	}

	renderGameHeader() {
		const {
			IsClosed,
			DateTime,
			Status,
			Quarter,
			TimeRemainingMinutes,
			TimeRemainingSeconds,
		} = this.props.teamBoxScore.Game;

		const { Name: ArenaName } = this.props.teamGameArena;

		return (
			<ScoreboardHeader
				date={DateTime}
				venue={ArenaName}
				isClosed={IsClosed}
				status={Status}
				quarter={Quarter}
				timeRemainingMinutes={TimeRemainingMinutes}
				timeRemainingSeconds={TimeRemainingSeconds}
			/>
		);
	}

	renderBoxScoreHeader() {
		const { Quarters } = this.props.teamBoxScore;
		return (
			<thead>
				<tr>
					<th />
					{Quarters.map(item => <th key={item.QuarterID}>{item.Name}</th>)}
					<th>T</th>
				</tr>
			</thead>
		);
	}

	renderBoxScoreTeams() {
		const { Quarters, Game } = this.props.teamBoxScore;
		return (
			<tbody>
				<tr>
					<th>{Game.AwayTeam}</th>
					{Quarters.map(item => <td key={item.QuarterID}>{item.AwayScore}</td>)}
					<td>{Game.AwayTeamScore}</td>
				</tr>
				<tr>
					<th>{Game.HomeTeam}</th>
					{Quarters.map(item => <td key={item.QuarterID}>{item.HomeScore}</td>)}
					<td>{Game.HomeTeamScore}</td>
				</tr>
			</tbody>
		);
	}

	renderTeamStats() {
		const { TeamGames } = this.props.teamBoxScore;
		return TeamGames.map(item => (
			<ScoreboardStatsProgress
				key={item.TeamID}
				homeOrAway={item.HomeOrAway.toLowerCase()}
				assists={item.Assists}
				rebounds={item.Rebounds}
				steals={item.Steals}
				blocks={item.BlockedShots}
				turnovers={item.Turnovers}
			/>
		));
	}

	render() {
		if (!this.props.teamBoxScoreLoaded) {
			return <p>loading</p>;
		}
		return (
			<div className="game-result">
				{this.renderGameHeader()}
				<section className="game-result__flex-row">
					<ScoreboardTeam
						isHomeTeam={false}
						teamLogo={espnLogo(this.props.teamBoxScore.Game.AwayTeam, 100)}
						teamCity={
							this.props.teamBoxScore.Game.AwayTeam === this.props.teamAbrv
								? this.props.teamCity
								: this.props.teamBoxScore.Game.AwayTeam
						}
						teamName={this.props.teamBoxScore.Game.AwayTeam === this.props.teamAbrv ? this.props.teamName : 'Other'}
						teamRecord="4-1, 2-0, away"
						teamGameScore={this.props.teamBoxScore.Game.AwayTeamScore}
						isWinner={
							this.props.teamBoxScore.Game.AwayTeamScore > this.props.teamBoxScore.Game.HomeTeamScore &&
							this.props.teamBoxScore.Game.IsClosed
						}
					/>
					<div className="game-result__score--wrap">
						<span className="game-result__score game-result__score--dash">-</span>
					</div>
					<ScoreboardTeam
						isHomeTeam
						teamLogo={espnLogo(this.props.teamBoxScore.Game.HomeTeam, 100)}
						teamCity={
							this.props.teamBoxScore.Game.HomeTeam === this.props.teamAbrv
								? this.props.teamCity
								: this.props.teamBoxScore.Game.HomeTeam
						}
						teamName={this.props.teamBoxScore.Game.HomeTeam === this.props.teamAbrv ? this.props.teamName : 'Other'}
						teamRecord="0-5, 0-2, Home"
						teamGameScore={this.props.teamBoxScore.Game.HomeTeamScore}
						isWinner={this.props.teamBoxScore.Game.HomeTeamScore > this.props.teamBoxScore.Game.AwayTeamScore}
					/>
				</section>

				<section className="game-result__flex-row">
					{this.renderTeamStats()}
					{/* <ScoreboardStatsProgress homeOrAway="away" /> */}
					{/* <BoxScore
						awayTeamAbrv="MIL"
						awayTeamQ1={28}
						awayTeamQ2={34}
						awayTeamQ3={26}
						awayTeamQ4={35}
						homeTeamAbrv="CHI"
						homeTeamQ1={35}
						homeTeamQ2={39}
						homeTeamQ3={33}
						homeTeamQ4={27}
					/> */}
					{/* <ScoreboardStatsProgress homeOrAway="home" /> */}
					<table className="table box-score">
						{this.renderBoxScoreHeader()}
						{this.renderBoxScoreTeams()}
					</table>
				</section>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	teamBoxScore: state.team.teamBoxScore,
	teamBoxScoreLoaded: state.team.teamBoxScoreLoaded,
	teamGameArena: state.teams.teamGameArena,
	teamGameArenaLoaded: state.teams.teamGameArenaLoaded,
});

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			getTeamGame,
			getArenaByStadiumId,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(TeamScoreboard);

TeamScoreboard.propTypes = {
	getTeamGame: PropTypes.func.isRequired,
	teamBoxScoreLoaded: PropTypes.bool,
	teamGameID: PropTypes.number,
	teamAbrv: PropTypes.string.isRequired,
	arenas: PropTypes.arrayOf(PropTypes.object).isRequired,
	teamName: PropTypes.string,
	teamCity: PropTypes.string,
	teamGameArenaLoaded: PropTypes.bool,
	getArenaByStadiumId: PropTypes.func.isRequired,
	teamGameArena: PropTypes.shape({
		Name: PropTypes.string,
	}),
	teamBoxScore: PropTypes.shape({
		Game: PropTypes.shape({
			AwayTeam: PropTypes.string,
			HomeTeam: PropTypes.string,
			AwayTeamScore: PropTypes.number,
			HomeTeamScore: PropTypes.number,
			IsClosed: PropTypes.bool,
			DateTime: PropTypes.string,
			Status: PropTypes.string,
			Quarter: PropTypes.string,
			TimeRemainingMinutes: PropTypes.number,
			TimeRemainingSeconds: PropTypes.number,
		}),
		TeamGames: PropTypes.arrayOf(PropTypes.object),
		Quarters: PropTypes.arrayOf(PropTypes.object),
	}).isRequired,
};

TeamScoreboard.defaultProps = {
	teamBoxScoreLoaded: false,
	teamGameArenaLoaded: false,
	teamName: '',
	teamCity: '',
	teamGameArena: {},
};
