import React from 'react';
import { ProgressBar } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { singleGameRecords } from '../../util/helpers';

const ScoreboardStatsProgress = props => (
	<div className={`boxscore__progress--wrap boxscore__progress--${props.homeOrAway}`}>
		<div className="boxscore__progress">
			<span className="boxscore__progress--label">ass</span>
			<ProgressBar
				now={(Number(props.assists) / singleGameRecords.assists).toFixed(2) * 100}
				label={Math.trunc(props.assists)}
			/>
			<span className="boxscore__progress--percent">{Math.trunc(props.assists)}</span>
		</div>
		<div className="boxscore__progress">
			<span className="boxscore__progress--label">reb</span>
			<ProgressBar now={(Number(props.rebounds) / singleGameRecords.rebounds).toFixed(2) * 100} />
			<span className="boxscore__progress--percent">{Math.trunc(props.rebounds)}</span>
		</div>
		<div className="boxscore__progress">
			<span className="boxscore__progress--label">ste</span>
			<ProgressBar now={(Number(props.steals) / singleGameRecords.steals).toFixed(2) * 100} />
			<span className="boxscore__progress--percent">{Math.trunc(props.steals)}</span>
		</div>
		<div className="boxscore__progress">
			<span className="boxscore__progress--label">blk</span>
			<ProgressBar now={(Number(props.blocks) / singleGameRecords.blocks).toFixed(2) * 100} />
			<span className="boxscore__progress--percent">{Math.trunc(props.blocks)}</span>
		</div>
		<div className="boxscore__progress">
			<span className="boxscore__progress--label">to</span>
			<ProgressBar now={(Number(props.turnovers) / singleGameRecords.turnovers).toFixed(2) * 100} />
			<span className="boxscore__progress--percent">{Math.trunc(props.turnovers)}</span>
		</div>
	</div>
);

ScoreboardStatsProgress.propTypes = {
	homeOrAway: PropTypes.string.isRequired,
	assists: PropTypes.number,
	rebounds: PropTypes.number,
	steals: PropTypes.number,
	blocks: PropTypes.number,
	turnovers: PropTypes.number,
};

ScoreboardStatsProgress.defaultProps = {
	assists: 0,
	rebounds: 0,
	steals: 0,
	blocks: 0,
	turnovers: 0,
};

export default ScoreboardStatsProgress;
