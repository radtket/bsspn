import React, { Component } from 'react';
import { Tabs, Tab, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import StandingsTeam from './StandingsTeam.jsx';
import { getStandings } from '../../actions';
import { smallestToLargest, winningPercentage } from '../../util/helpers';

class Standings extends Component {
	componentWillMount() {
		this.props.getStandings();
		// if (!this.props.standingsLoaded) {
		// 	this.props.getStandings();
		// }
	}

	renderConferenceStandings() {
		const { standings, teamConference, currentTeam } = this.props;
		return standings
			.filter(item => item.Conference === teamConference)
			.sort(smallestToLargest('GamesBack'))
			.map(team => (
				<StandingsTeam
					activeTeam={currentTeam}
					key={team.TeamID}
					teamName={team.Name}
					teamLogo={team.Key}
					wins={team.Wins}
					loses={team.Losses}
					gb={team.GamesBack ? (Math.round(`${team.GamesBack}` * 2) / 2).toFixed(1) : '-'}
				/>
			));
	}

	renderDivisionStandings() {
		const { standings, teamConference, teamDivision, currentTeam } = this.props;

		const divStandings = [...standings]
			.filter(item => item.Conference === teamConference && item.Division === teamDivision)
			.sort(winningPercentage('Wins', 'Losses'));

		return divStandings
			.reduce((accumulator, team) => {
				accumulator.push({
					TeamID: team.TeamID,
					Key: team.Key,
					Name: team.Name,
					Wins: team.Wins,
					Losses: team.Losses,
					GamesBehind: divStandings[0].Wins - team.Wins + (team.Losses - divStandings[0].Losses) / 2,
				});
				return accumulator;
			}, [])
			.map(team => (
				<StandingsTeam
					activeTeam={currentTeam}
					key={team.TeamID}
					teamName={team.Name}
					teamLogo={team.Key}
					wins={team.Wins}
					loses={team.Losses}
					gb={team.GamesBehind !== 0 || null ? team.GamesBehind.toString() : '-'}
				/>
			));
	}

	render() {
		return (
			<Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
				<Tab eventKey={1} title="division">
					<Table className="standings" responsive>
						<thead>
							<tr>
								<th>{this.props.teamDivision}</th>
								<th>W</th>
								<th>L</th>
								<th>GB</th>
							</tr>
						</thead>
						<tbody>{this.renderDivisionStandings()}</tbody>
					</Table>
				</Tab>
				<Tab eventKey={2} title="conference">
					<Table className="standings standings__conference" responsive>
						<thead>
							<tr>
								<th>{this.props.teamConference}</th>
								<th>W</th>
								<th>L</th>
								<th>GB</th>
							</tr>
						</thead>
						<tbody>{this.renderConferenceStandings()}</tbody>
					</Table>
				</Tab>
			</Tabs>
		);
	}
}

const mapStateToProps = state => ({
	standings: state.teams.standings,
	standingsLoaded: state.teams.standingsLoaded,
});

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			getStandings,
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(Standings);

Standings.propTypes = {
	getStandings: PropTypes.func.isRequired,
	standingsLoaded: PropTypes.bool,
	standings: PropTypes.arrayOf(PropTypes.object).isRequired,
	teamConference: PropTypes.string.isRequired,
	teamDivision: PropTypes.string.isRequired,
	currentTeam: PropTypes.string,
};

Standings.defaultProps = {
	standingsLoaded: false,
	currentTeam: '',
};
