import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { espnLogo } from '../../util/helpers';

const StandingsTeam = props => (
	<tr className={props.teamLogo === props.activeTeam ? 'standings__active-team' : ''}>
		<td>
			<Link to={`/team/${props.teamLogo}`}>
				<figure className="standings__team--logo">
					<img
						src={props.teamLogo === 'UTA' ? espnLogo('UTAH', 24) : espnLogo(`${props.teamLogo}`, 24)}
						alt={`${props.teamName} Logo`}
					/>
				</figure>
				<figcaption className="standings__team--name">{props.teamName}</figcaption>
			</Link>
		</td>
		<td>{props.wins}</td>
		<td>{props.loses}</td>
		<td>{props.gb}</td>
	</tr>
);

export default StandingsTeam;

StandingsTeam.propTypes = {
	teamLogo: PropTypes.string.isRequired,
	teamName: PropTypes.string.isRequired,
	wins: PropTypes.number.isRequired,
	loses: PropTypes.number.isRequired,
	gb: PropTypes.string,
	activeTeam: PropTypes.string,
};

StandingsTeam.defaultProps = {
	gb: '-',
	activeTeam: '',
};
