import React from 'react';
import PropTypes from 'prop-types';

const Card = props => (
	<article className="card">
		<header className="card__header">
			<h3>{props.title}</h3>
		</header>
		<div className="card__content">{props.component}</div>
	</article>
);

Card.propTypes = {
	title: PropTypes.string.isRequired,
	component: PropTypes.element.isRequired,
};

export default Card;
