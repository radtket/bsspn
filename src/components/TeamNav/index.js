import React from 'react';
import { Nav, NavItem } from 'react-bootstrap';

const TeamNav = () => (
	<section className="team-nav">
		<div className="container">
			<Nav bsStyle="pills" justified activeKey={1} onSelect={key => this.handleSelect(key)}>
				<NavItem eventKey={1} href="/home">
					home
				</NavItem>
				<NavItem eventKey={2} title="Item">
					stats
				</NavItem>
				<NavItem eventKey={3}>schedule</NavItem>
				<NavItem eventKey={4} title="Item">
					roster
				</NavItem>
				<NavItem eventKey={5}>stadium</NavItem>
			</Nav>
		</div>
	</section>
);

export default TeamNav;
