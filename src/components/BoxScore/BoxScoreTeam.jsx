import React from 'react';
import PropTypes from 'prop-types';

const BoxScoreTeam = props => (
	<tr>
		<th>{props.TeamAbrv}</th>
		<td>{props.Q1}</td>
		<td>{props.Q2}</td>
		<td>{props.Q3}</td>
		<td>{props.Q4}</td>
		<td>{props.Q1 + props.Q2 + props.Q3 + props.Q4}</td>
	</tr>
);

export default BoxScoreTeam;

BoxScoreTeam.propTypes = {
	TeamAbrv: PropTypes.string.isRequired,
	Q1: PropTypes.number,
	Q2: PropTypes.number,
	Q3: PropTypes.number,
	Q4: PropTypes.number,
};

BoxScoreTeam.defaultProps = {
	Q1: '',
	Q2: '',
	Q3: '',
	Q4: '',
};
