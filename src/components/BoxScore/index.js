import React from 'react';
import PropTypes from 'prop-types';
import BoxScoreTeam from './BoxScoreTeam.jsx';

const BoxScore = props => (
	<table className="table box-score">
		<thead>
			<tr>
				<th />
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>F</th>
			</tr>
		</thead>
		<tbody>
			<BoxScoreTeam
				TeamAbrv={props.awayTeamAbrv}
				Q1={props.awayTeamQ1}
				Q2={props.awayTeamQ2}
				Q3={props.awayTeamQ3}
				Q4={props.awayTeamQ4}
			/>
			<BoxScoreTeam
				TeamAbrv={props.homeTeamAbrv}
				Q1={props.homeTeamQ1}
				Q2={props.homeTeamQ2}
				Q3={props.homeTeamQ3}
				Q4={props.homeTeamQ4}
			/>
		</tbody>
	</table>
);

export default BoxScore;

BoxScore.propTypes = {
	awayTeamAbrv: PropTypes.string.isRequired,
	homeTeamAbrv: PropTypes.string.isRequired,
	awayTeamQ1: PropTypes.number,
	awayTeamQ2: PropTypes.number,
	awayTeamQ3: PropTypes.number,
	awayTeamQ4: PropTypes.number,
	homeTeamQ1: PropTypes.number,
	homeTeamQ2: PropTypes.number,
	homeTeamQ3: PropTypes.number,
	homeTeamQ4: PropTypes.number,
};

BoxScore.defaultProps = {
	awayTeamQ1: '',
	awayTeamQ2: '',
	awayTeamQ3: '',
	awayTeamQ4: '',
	homeTeamQ1: '',
	homeTeamQ2: '',
	homeTeamQ3: '',
	homeTeamQ4: '',
};
