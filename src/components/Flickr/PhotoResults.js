import React from 'react';
import PropTypes from 'prop-types';

class PhotoResults extends React.Component {
	render() {
		const { data } = this.props;
		if (data.length <= 0) {
			return <p className="hidden">No Photos</p>;
		}
		return data.map(photo => {
			const { id, farm, server, secret, title } = photo;
			const source = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`;
			return (
				<a href={source} key={id} target="_blank">
					<img src={source} alt={title} className="img-responsive" />
				</a>
			);
		});
	}
}

PhotoResults.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object),
};

PhotoResults.defaultProps = {
	data: [],
};

export default PhotoResults;
