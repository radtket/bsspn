import React from 'react';
import PropTypes from 'prop-types';

class SearchBar extends React.Component {
	handleSubmit = e => {
		e.preventDefault();
		const apiKey = '91a0a27bed2c53729eb28a50981edc78';
		const searchKeyword = this.photoKeyword.value;
		this.photoKeyword.value = '';

		const url = `https://api.flickr.com/services/rest/?api_key=${apiKey}&method=flickr.photos.search&format=json&nojsoncallback=1&&per_page=50&page=1&text=${searchKeyword}`;

		fetch(url)
			.then(response => response.json())
			.then(data => {
				this.props._getPhotos(data.photos.photo);
			})
			.catch(error => {
				throw error;
			});
	};

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<input
					type="text"
					placeholder="Search keyword..."
					ref={c => {
						this.photoKeyword = c;
					}}
					required
				/>
				<button
					type="submit"
					ref={c => {
						this.button = c;
					}}
				>
					Search on Flickr
				</button>
			</form>
		);
	}
}

export default SearchBar;

SearchBar.propTypes = {
	_getPhotos: PropTypes.func.isRequired,
};
