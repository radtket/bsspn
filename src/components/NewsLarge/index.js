import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const NewsLarge = props => (
	<article className="card news-lg">
		<figure>
			<a href={props.newsLink} target="_blank">
				<img src={props.newsImage} alt={props.newsImageDisc} />
				<i className="fa fa-link" />
			</a>
		</figure>
		<div className="card__content">
			<time dateTime={moment(props.dateTime).format()}>{moment(props.newsDate).format('MMMM Do YYYY')}</time>
			<a href={props.newsLink} target="_blank">
				<h3>{props.newsHeadline}</h3>
			</a>
			<p>{props.newsBody}</p>
		</div>
	</article>
);

export default NewsLarge;

NewsLarge.propTypes = {
	dateTime: PropTypes.instanceOf(Date),
	newsBody: PropTypes.string.isRequired,
	newsDate: PropTypes.string.isRequired,
	newsHeadline: PropTypes.string.isRequired,
	newsImage: PropTypes.string,
	newsLink: PropTypes.string.isRequired,
	newsImageDisc: PropTypes.string,
};

NewsLarge.defaultProps = {
	newsImageDisc: '',
	dateTime: new Date(),
	newsImage: 'http://via.placeholder.com/1600x900',
};
