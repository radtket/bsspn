import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import { themeColors } from '../../util/helpers';

const TeamHero = props => (
	<HeroWrapper backdrop={props.teamLogo} backdropColor={props.teamColor} className="small-section page-heading">
		<div className="container">
			<Grid>
				<Row className="show-grid">
					<Col xs={12} md={8}>
						<HeroHeadline>
							<span>{props.teamCity} </span>
							{props.teamName}
						</HeroHeadline>
					</Col>
				</Row>
			</Grid>
		</div>
	</HeroWrapper>
);

TeamHero.propTypes = {
	teamCity: PropTypes.string,
	teamName: PropTypes.string,
	teamLogo: PropTypes.string,
	teamColor: PropTypes.string,
};

TeamHero.defaultProps = {
	teamCity: '',
	teamName: '',
	teamLogo: '',
	teamColor: `${themeColors.lime}`,
};

const HeroWrapper = styled.section`
	background-color: #${props => props.backdropColor};
	background: -webkit-gradient(
				linear,
				left top,
				left bottom,
				from(#${props => props.backdropColor}),
				color-stop(38%, #${props => props.backdropColor}),
				to(transparent)
			)
			no-repeat,
		${themeColors.black};
	background: -webkit-linear-gradient(
				top,
				#${props => props.backdropColor} 0%,
				#${props => props.backdropColor} 38%,
				transparent 100%
			)
			no-repeat,
		${themeColors.black};
	background: -o-linear-gradient(
				top,
				#${props => props.backdropColor} 0%,
				#${props => props.backdropColor} 38%,
				transparent 100%
			)
			no-repeat,
		${themeColors.black};
	background: linear-gradient(
				to bottom,
				#${props => props.backdropColor} 0%,
				#${props => props.backdropColor} 38%,
				transparent 100%
			)
			no-repeat,
		${themeColors.black};

	&:after {
		content: '';
		background: url(${props => props.backdrop}) center center/100% no-repeat;
		opacity: 0.2;
		filter: alpha(opacity=20);
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		position: absolute;
		z-index: 0;
		background-repeat: no-repeat;
		background-position: center center;
		background-size: 50%;
	}
`;

const HeroHeadline = styled.h1`
	color: ${themeColors.white};
	position: relative;
	z-index: 1;
`;

export default TeamHero;
