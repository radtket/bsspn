import React from 'react';
import SearchBar from './Flickr/SearchBar';
import PhotoResults from './Flickr/PhotoResults';

class PhotosComponent extends React.Component {
	state = {
		photos: [],
	};

	handleGetPhotos = photos => {
		this.setState({ photos });
	};

	render() {
		return (
			<div>
				<SearchBar _getPhotos={this.handleGetPhotos} />
				<PhotoResults data={this.state.photos} />
			</div>
		);
	}
}

export default PhotosComponent;
